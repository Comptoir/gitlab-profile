Several free software created by [Adullact](https://adullact.org) and available under the [GNU AGPL v3](https://www.gnu.org/licenses/agpl-3.0) license.

[ToC]

--------------------------------------

## Comptoir du Libre, v2 (current version)

[![Project badge](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/badges/release.svg)](https://gitlab.adullact.net/Comptoir/Comptoir-srv/-/releases)
[![Project badge](https://gitlab.adullact.net/Comptoir/Comptoir-srv/badges/main/pipeline.svg)](https://gitlab.adullact.net/Comptoir/Comptoir-srv/pipelines?ref=main) 

The **Comptoir du Libre** is a website that lists free software useful to public services, their users and service providers.
As a collaborative platform, you can easily share experiences, find tools and identify partners.

- [`Comptoir du Libre`, **source code**](https://gitlab.adullact.net/Comptoir/Comptoir-srv)
- [`Comptoir du Libre`, website](https://comptoir-du-libre.org/fr/)

--------------------------------------

## Comptoir du Libre, v3 (upcoming version)

[![Project badge](https://gitlab.adullact.net/Comptoir/comptoir-du-libre/-/badges/release.svg)](https://gitlab.adullact.net/Comptoir/comptoir-du-libre/-/releases)
[![Project badge](https://gitlab.adullact.net/Comptoir/comptoir-du-libre/badges/main/pipeline.svg)](https://gitlab.adullact.net/Comptoir/comptoir-du-libre/pipelines?ref=main) 

Upcoming source code (v3) of [comptoir-du-libre.org](https://comptoir-du-libre.org/fr/) website.

- [`Comptoir du Libre` next major version, **source code**](https://gitlab.adullact.net/Comptoir/comptoir-du-libre)

--------------------------------------

## Puppet module

[![Project badge](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/badges/release.svg)](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/releases) 
[![Project badge](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/badges/main/pipeline.svg)](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/pipelines?ref=main) 
[![Puppet Tajine downloads](https://img.shields.io/puppetforge/dt/adullact/comptoir?label=Puppet%20Comptoir%20downloads)](https://forge.puppet.com/modules/adullact/comptoir/readme)

 - [**Comptoir du Libre** Puppet module, **source code**](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre)
 - [**Comptoir du Libre** Puppet module on **Puppet Forge**](https://forge.puppet.com/modules/adullact/comptoir/readme)


--------------------------------------

## Vagrant VM 

> ⚠️ Warning : only for test or demonstration purposes

[![Project badge](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/-/badges/release.svg)](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/-/releases)
[![Project badge](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/badges/main/pipeline.svg)](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/pipelines?ref=main) 

Build from scratch a running **Comptoir du Libre** webapp in a VM on your laptop.

- [`vagrant-comptoir-du-libre`, **source code**](https://gitlab.adullact.net/Comptoir/vagrant-comptoir-du-libre/)

